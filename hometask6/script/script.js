//************************ЗАДАЧА***************************

// Реализовать функцию-конструктор для создания объекта "пользователь".
//
//     Технические требования:
//
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект "пользователь".
//
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//
//     Используя данные, введенные пользователем, создать объект со свойствами firstName и lastName.
//
//     Добавить в объект метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную
//     с фамилией пользователя, все в нижнем регистре.
//
//     Не обязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры
//     setFirstName() и setLastName(), которые позволят изменить данные свйоства.

//**************************АЛГОРИТМ ВЫПОЛНЕНИЯ ЗАДАЧИ**************************
//1.Написать функцию createNewUser().
//2.Спросить и сохранить имя пользователя.
//3.Cпросить и сохранить фамилию пользователя.
// 4. Создать и сохранить пустой объект пользователь.
//5.создать и сохранить своиства которые будут добавлены в объект пользователь
//6.Добавить метод getLogin() в объект c переменными yourName, yourSurname
//7.Создать переменную и записать результат соединения(используя шаблонную строку)
// первой буквы имени (используя метод для строк CharAt) и фамилии.
//8.Алертом вывести соединную строку в нижнем регистре( используя метод toLowerCase).
//9.Cоздать функцию сеттер setFirstName()
//10.Создать функцию setLastName()


function createNewUser(){

    Object.defineProperty(this, 'firstName', {value: prompt("Ваше имя",""), configurable: true})
    Object.defineProperty(this, 'yourSurname', {value: prompt("Ваша фамилия",""), configurable: true})


    this.getLogin = function(){
         let joinName = (`${this.firstName.charAt(0)}${this.yourSurname}`);
        alert(joinName.toLowerCase())};
    this.getLogin();
 return this
}

let newUser = new createNewUser();

console.log(newUser)


