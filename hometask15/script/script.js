// ЗАДАНИЕ
//
//
//
// Данное задание не обязательно для выполнения
//
// Реализовать секундомер с точностью до миллисекунд.
//
//     Технические требования:
//
//     При запуске программы на экране должно быть табло с минутами, секундами и миллисекундами, а также две кнопки - Start и Clear.
//
//     При нажатии на кнопку Start - запускать отсчет секундомера. При этом вместо кнопки Start должна появиться кнопка Pause.
//
//     Нажатие кнопки Pause временно останавливает отсчет, до повторного нажатия кнопки Start.
//
//     Нажатие кнопки Clear останавливает отсчет (если он был запущен) и обнуляет таймер.
//
//     Не обязательное задание продвинутой сложности:
//
//     Реализовать таймер не с электронным, а циферблатным вариантом отображения. Миллисекунды должны быть показаны в отдельном маленьком циферблате.



//!!!!!!!!!!!!!!!!!!!!!!  ВАЖНОЕ ЗАМЕЧАНИЕ:  ФУНКЦИЯ  clearInterval  НЕ ОБНУЛЯЕТ ТАЙМЕР,  А ПРЕРЫВАЕТ ЕГО ВЫПОЛНЕНИЕ ТОЕСТЬ КАК БЫ СТАВИТ ПАУЗУ В ЕГО ВЫПОЛНЕНИИ.

let hourCounter = 0
let minuteCounter  = 0;
let secondCounter = 0;
let milisecondCounter = 0;

let hourDigit = document.getElementById('hours')
let minuteDigit = document.getElementById('minute')
let secondDigit = document.getElementById('second')
let miliSecondDigit = document.getElementById('milisecond')
let startbtn = document.getElementById('start')
let pausetbtn = document.getElementById('pause')
let resetbtn = document.getElementById('clear')

startbtn.addEventListener('click',timerHandler)

function formatTime (timeElement, htmlElement){
    if(timeElement < 10){
        htmlElement.innerText = `0${timeElement}`
    } else {
        htmlElement.innerText = timeElement
    }
}

function timer(){
    milisecondCounter++;

    formatTime(milisecondCounter, miliSecondDigit);

    if(milisecondCounter === 99) {
        milisecondCounter = 0
        secondCounter++
    }

    formatTime(secondCounter, secondDigit);

    if(secondCounter===59) {
        secondCounter = 0
        minuteCounter++
    }

    formatTime(minuteCounter, minuteDigit);
}

function timerHandler(){

    let timerId = setInterval(timer, 10);
    startbtn.className = "d-none"
    pausetbtn.className="pause";

    function pause(){
        clearInterval(timerId);
        startbtn.className = "start"
        pausetbtn.className="d-none";
    }

    pausetbtn.addEventListener('click',pause);

    function btnReset() {
        minuteCounter  = 0;
        secondCounter = 0;
        milisecondCounter = 0;
        miliSecondDigit.innerText = `0${milisecondCounter}`
        secondDigit.innerText = `0${secondCounter}`
        minuteDigit.innerText = `0${minuteCounter}`
        clearInterval(timerId);
        startbtn.className = "start"
        pausetbtn.className="d-none";
    }
    resetbtn.addEventListener('click',btnReset)
}





