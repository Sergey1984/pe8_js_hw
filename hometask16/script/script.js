// ЗАДАНИЕ
//
//
//
// Данное задание не обязательно для выполнения
//
// Реализовать возможность смены цветовой темы сайта пользователем.
//
//     Технические требования:
//
//     Взять любое готовое домашнее задание по HTML/CSS.
//
//     Добавить на макете кнопку "Сменить тему".
//
//     При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.)
//     на ваше усмотрение. При повтором нажатии - возвращать все как было изначально -
//     как будто для страницы доступны две цветовых темы.
//
//     Выбранная тема должна сохраняться и после перезагрузки страницы.

//**********************************************Агоритм
//Вешаем клик на кнопу "Сменить тему"  в html
// В цсс задаем стили которые применяться при нажатии кнопки.
// Сохранить  в переменную елемент стили  которого мы хотим поменять
// Cохраняем в переменную своиство которое мы хотим изменить
// Сделать проверку текущего стиля при клике.


//сохранить в переменную mainСontent объект main-content
// сохранить в сторедж сцц своиства main-content
//



let mainContent = document.getElementsByClassName("main-content")[0]

const ready = function(){
let activeStyle = localStorage.getItem('activeStyle')
if (activeStyle == null || activeStyle === 'main-content') {
    mainContent.className = "main-content"
} else {
    mainContent.className = "change-main-content"
}
}
document.addEventListener('DOMContentLoaded',ready);

let btnTheme = document.getElementsByClassName('btn-tema')[0]
console.log(btnTheme)
btnTheme.onclick = function () {

    let activeClass = mainContent.className;

    if (mainContent.className === "main-content") {

        localStorage.setItem('activeStyle', 'change-main-content')
        mainContent.className = "change-main-content"
    } else {
        localStorage.setItem('activeStyle', 'main-content')
        mainContent.className = "main-content"
    }

}




//Вариант1:


// let mainContent = document.getElementsByClassName("main-content")[0]
//
// let activeStyle = localStorage.getItem('activeStyle')
// if (activeStyle == null || activeStyle === 'main-content') {
//     mainContent.className = "main-content"
// } else {
//     mainContent.className = "change-main-content"
// }
//
//
// let btnTheme = document.getElementsByClassName('btn-tema')[0]
// console.log(btnTheme)
// btnTheme.onclick = function () {
//
//     let activeClass = mainContent.className;
//
//     if (mainContent.className === "main-content") {
//
//         localStorage.setItem('activeStyle', 'change-main-content')
//         mainContent.className = "change-main-content"
//     } else {
//         localStorage.setItem('activeStyle', 'main-content')
//         mainContent.className = "main-content"
//     }
//
// }

//Вариант2

// let mainContent = document.getElementsByClassName("main-content")[0]
//
// let activeStyle = localStorage.getItem('activeStyle')
// if (activeStyle == null || activeStyle == 'default') {
//     mainContent.style.backgroundColor = ""
// } else {
//     mainContent.style.backgroundColor = "red"
// }
//
//
// let btnTheme = document.getElementsByClassName('btn-tema')[0]
//
// btnTheme.onclick = function () {
//
//     let mainContent = document.getElementsByClassName("main-content")[0]
//     let colorbg = mainContent.style.backgroundColor
//
//     if (mainContent.style.backgroundColor !== "red") {
//         localStorage.setItem('activeStyle', 'red')
//         mainContent.style.backgroundColor = "red"
//     } else {
//         localStorage.setItem('activeStyle', 'default')
//         mainContent.style.backgroundColor = ""
//     }
//
// }



