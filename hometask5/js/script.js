//**********ЗАДАЧА: КЛОН ОБЪЕКТА РЕКУРСИЕЙ***************
// **************Алгоритм решения задачи по клонированию обьекта*****************
// 1.Сделать рекурсивно
//2.Написать функцию которая на вход будет получать объект().
//3.создать пустой обект
//4.for in перебрать входной объект
//5.сохраняем значение своиста объекта object в переменную
//6.Создать ветвление: До тех пор пока в осходном объекте будет объект типа (typeof) объект то
// в своиство нового обекта присвоить значение вызова функции c аргументом исходного объекта.

let object = {
    name:"sergey",
    age:30,
    adress: {
        street: "street",
        number:25,
    },

    work:{
        city:"Kiev",
        company:"globex",
        people:30
    }
}


// function cloneObj(obj) {
//     let output = {}
//     if(typeof obj !== "object" || obj == null){
//         return obj
//     }
//
//     for(let key in obj){
//         output[key] = cloneObject(obj[key])
//     }
//
//     return output
// }

function copy(o) {

    let output = {};//Создание пустого объекта

    for (let key in o) {
       let v = o[key];//сохраняем значение своиста объекта object в переменную v
        if (typeof v === "object" || v == null){
            output[key] = copy(v);
        }else {
            output[key] = v;
        }

        // output[key] = (typeof v === "object") ? copy(v) : v;  //запись равносильна коду с 38- 41 строк.
    }
    return output;
}

// copy(obj);  Вызов функции сopy аргуметом котой есть объект obj

// let cloneObject = obj;  // при таком коде будет скопированна ссылка. Полное копирование не будут выполнены.
// Изменения в скопированом объекте будет приводить к изменениям в обоих объектах.

let cloneObject = copy(object);//при таком коде будет вызвана функция результат  выполнения которой запишется в obj2cloneObject.
// cloneObject будет клоном obj
cloneObject.name = "Ivan";
cloneObject.age = 18;
cloneObject.car = "mazda 6 sport";

console.log(object);
console.log(cloneObject);








